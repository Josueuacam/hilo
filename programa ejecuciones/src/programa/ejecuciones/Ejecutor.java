/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package programa.ejecuciones;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CHARL
 */
public class Ejecutor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int op = 0;
        System.out.println("Equipo: josue,armando y nardin");

        do {
            System.out.println("---------------------------------------------------------");
            System.out.println("Ingrese una opccion");
            System.out.println("1.- Para programa con Thread");
            System.out.println("2.- Para programa run Runnable");
            System.out.println("3.- para programa sin Thread ni Runneable");
            System.out.println("4 - 9 Para salir");
            try {
                op = sc.nextInt();
            } catch (Exception e) {
                System.out.println(e);
            }
            System.out.println("---------------------------------------------------------");
            switch (op) {
                case 1:

                    for (int i = 1; i <= 3; i++) {
                        ClaseThread hilo = new ClaseThread(i);
                        hilo.start();
                    }
                    break;
                case 2:

                    for (int i = 1; i <= 3; i++) {
                        ClaseRunnable runnable = new ClaseRunnable(i);
                        Thread run = new Thread(runnable);
                        run.start();
                    }

                    break;
                case 3:
                    for (int i = 1; i <= 3; i++) {
                        ClaseSimple sim = new ClaseSimple(i);
                        sim.sinHilos();
                    }

                    break;
                default:
                    System.out.println("Esta saliendo del programa");
                    break;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Ejecutor.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (((op == 1) || (op == 2) || (op == 3)));
    }

}
